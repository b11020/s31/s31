const express = require("express");
const mongoose = require("mongoose");

// this allow us to use all the routes defined in the taskRoute.js
const taskRoute = require("./Routes/taskRoute");

// setup express server
const app = express();
const port = 3001;

// middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// mongoDB connection string
mongoose.connect("mongodb+srv://admin:admin@cluster0.2cnfh.mongodb.net/b183_to-do?retryWrites=true&w=majority",
{useNewUrlParser: true, useUnifiedTopology: true});

// set notification for connection or failure
let db = mongoose.connection;

// notify error
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));


// Add the taskRoute
// allow all the task routes created in the "taskRoutes.js" file to use "/task"

// localhost:3001/tasks
app.use("/tasks", taskRoute);

// server listening
app.listen(port, () => console.log(`Now listening to port ${port}`));

/*
	Separation of concerns

	Model Folder
	-contains objects and Schemas and defines the object structure and content

	Controllers
	-Business logic and JS applications

	Routes
	-will contain all the endpoints and assign the http methods for our application
	app.get("/",)
	-we seperate the routes such that the server "index.js" only contains info
	on the server.

	JS Modules
	require => to include a specific module / package.
	export.module => to treat a value as a package that can be used by other files

	Flow of exports and require
	-export models > require in controllers
	export controllers > requires in routes
	export routes > require in server (index.js)


*/

























