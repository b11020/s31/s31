const express = require("express");
// task controller allow us to use the function defined inside it.
const taskController = require ("../controllers/taskController");
// allows access to http method middlewares that makes it easier to create 
// routes for application.
const router = express.Router();

// Route to get all tasks 
router.get("/",(req,res)=>{

	taskController.getAllTasks().then(resultFromController => res.send(
		resultFromController));
})

// Route to Create a task 
// localhost:3001/tasks = it is the same with localhost:30001/tasks/
router.post("/",(req, res) => {
	// the createTask function needs data from the requestbody, so we need it to supply 
	// in the taskController.createTask(argument)
	taskController.createTask(req.body).then(resultFromController => res.send(
		resultFromController));
})



// route to DELETE a task
// endpoint /:id
// colon (:) is an identifier that helps create a dynamic route w/c allows us to supply info in the url
// Ex: localhost:30001/tasks/:id
router.delete("/:id", (req, res) => {
			// if info will be coming from the url the data can be accessed from the request params property
	taskController.deleteTask(req.params.id).then(resultFromController => res.
		send(resultFromController));
})

// route to update a task

router.patch("/:id",(req, res) =>{
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.
		send(resultFromController));
})


// ==================================================================================
router.get("/:id", (req, res)=>{

	taskController.getSpecificTasks(req.params.id).then(resultFromController => res.
		send(resultFromController));
})



router.put("/:id/complete", (req, res) =>{
	taskController.updateTaskStatus(req.params.id).then(resultFromController => res.
		send(resultFromController));
})


// Use "module.exports" to export the router object to be use in the server 
module.exports = router;






















