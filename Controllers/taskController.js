// it will allow us to use the contents of the task.js file in the model folder
const Task = require("../models/task");

module.exports.getAllTasks = () =>{

	// then is used to wait for the mongoose "find" method to finish 
	// before sending the result back to the route and eventually to the client/postman
	return Task.find({}).then(result => result);	
}
// create a task ===========================================
module.exports.createTask = (reqBody) =>{

		// Create task object base on the mongoose model "Task"
		let newTask = new Task({

			// sets the "name" property with the value received from postman
			name: reqBody.name
		})
			// Using callback function: newUSer.save((saveErr, savedTask) => {})
			// using .then method: newTask.save().then((Task, error) => {})
		return newTask.save().then((task, error) =>{
			if(error){
				console.log(error)
				return false
			}
			else{
				// returns the new task object save in the databse to then postman 
				return task
			}
		})
}
// DELETE a task================================================ 
// Business Logic
		/*
			1. Look for the task with the corresponding id provided in the URL/route
			2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
		*/
module.exports.deleteTask = (taskId) =>{
	return Task.findByIdAndRemove(taskId).then((removedTask, err) =>{
		if(err){
			console.log(err);
			return false;
		}
		else{
			return removedTask;
		}
	})
}
// Update a task==================================================== 
// Business Logic
		/*
			1. Get the task with the id using the Mongoose method "findById"
			2. Replace the task's name returned from the database with the "name" property from the request body
			3. Save the task
		*/
module.exports.updateTask = (taskId, reqBody) =>{
		// find by id is the same as "find({"_id":"value"})"
	return Task.findById(taskId).then((result, error) =>{
		if(error){
			console.log(error);
			return false;
		}
		else{
			// we re-assign the result name with the reqBody content
			result.name = reqBody.name;

			return result.save().then((updatedTaskName, updateErr) =>{
				if(updateErr){
					console.log(updateErr);
					return false;
				}
				else{
					return updatedTaskName;
				}
			})
		}
	})
}
// ======================================================================
module.exports.getSpecificTasks = (taskId) =>{
	
	return Task.findById(taskId).then((result, error) =>{
		if(error){
			console.log(error);
			return false;
		}
		else{
			return result;
		}
			
		})
}

// ==================================================================================
module.exports.updateTaskStatus = (taskId) => {
	return Task.findById(taskId).then((result, error) =>{
		if(error){
			console.log(error);
			return false;
		}

		else{
			result.status = "Complete";

			return result.save().then((updatedTaskStatus, updateErr) =>{

			if(updateErr){
			  console.log(updateErr)
			  return false
				}

				else{
					return updatedTaskStatus;
				}
			})
		}
	})
}






















